package org.geekbang.consistenthash;

import java.util.Random;

/**
 * 随机key生成器
 */
public class RandomKeyGenerator
{
    private static final String[] codeBase = {"1", "2", "3", "4", "5", "6", "7", "8", "9", "0", "a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z", "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z"};

    /**
     * 随机生成一个10位的key
     * @return
     */
    public static String nextKey()
    {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < 10; i++)
        {
            Random random = new Random();
            int index = random.nextInt(codeBase.length);
            sb.append(codeBase[index]);
        }
        return sb.toString();
    }
}
