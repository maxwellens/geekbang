package org.geekbang.consistenthash;

import java.util.Collection;
import java.util.List;

/**
 * 服务器路由接口
 */
public interface ServerRouter
{
    /**
     * 添加服务器
     *
     * @param server
     */
    void addServer(String server);

    /**
     * 批量添加服务器
     * @param servers
     */
    void addServers(Collection<String> servers);

    /**
     * 移除服务器
     *
     * @param server
     */
    void removeServer(String server);

    /**
     * 服务器列表
     *
     * @return
     */
    List<String> getServers();

    /**
     * 根据key得到服务器
     *
     * @param key
     * @return
     */
    String getServer(String key);

    /**
     * 计算标准差
     *
     * @return
     */
    int getStandardDeviation();
}
