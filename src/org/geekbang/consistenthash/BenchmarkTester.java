package org.geekbang.consistenthash;

import java.util.ArrayList;
import java.util.List;

/**
 * 基准测试类
 */
public class BenchmarkTester
{
    public static void main(String[] args)
    {
        //初始化服务器节点
        List<String> servers = new ArrayList<>();
        for (int i = 1; i <= 10; i++)
        {
            String server = "192.168.1." + i;
            servers.add(server);
        }
        //虚拟节点增加步长为10，虚拟节点数逐步增加
        for (int i = 1; i <= 30; i++)
        {
            testServerRouter(servers, 10 * i);
        }
    }

    /**
     * 测试单个路由器
     *
     * @param servers
     * @param virtualNodesPerServer
     */
    private static void testServerRouter(List<String> servers, Integer virtualNodesPerServer)
    {
        //服务器路由器
        ServerRouter router = new VirtualNodeConsistentHashServerRouter(virtualNodesPerServer);
        router.addServers(servers);
        for (int i = 0; i < 1_000_000; i++)
        {
            String key = RandomKeyGenerator.nextKey();
            String server = router.getServer(key);
            //System.out.println(key + "->" + server);
        }
        //格式：节点数：key统计数
        System.out.println(virtualNodesPerServer + ":" + router.getStandardDeviation());
    }
}
