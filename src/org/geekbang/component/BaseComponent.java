package org.geekbang.component;

/**
 * 通用组件类
 */
public abstract class BaseComponent implements Component
{
    protected String name;

    public BaseComponent(String name)
    {
        this.name = name;
    }

    @Override
    public String getName()
    {
        return name;
    }

    @Override
    public void print()
    {
        System.out.println("print " + this.getClass().getSimpleName() + "(" + name + ")");
    }

}
