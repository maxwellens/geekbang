package org.geekbang.component;

/**
 * 文本框
 */
public class TextBox extends BaseComponent
{
    protected String text;

    public TextBox(String name)
    {
        super(name);
    }

    public void setText(String text)
    {
        this.text = text;
    }

    public String getText()
    {
        return text;
    }
}
