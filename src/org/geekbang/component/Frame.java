package org.geekbang.component;

/**
 * 框架
 */
public class Frame extends ComponentGroup
{
    private String title;

    public Frame(String name)
    {
        super(name);
    }

    public void setTitle()
    {
        this.title = title;
    }

    public String getTitle()
    {
        return title;
    }
}
