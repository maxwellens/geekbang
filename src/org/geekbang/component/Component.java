package org.geekbang.component;

/**
 * 组件接口
 */
public interface Component
{
    String getName();

    void print();
}
