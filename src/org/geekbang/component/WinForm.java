package org.geekbang.component;

/**
 * 登录窗体
 */
public class WinForm extends ComponentGroup
{
    public WinForm(String name)
    {
        super(name);
    }

    public static void main(String[] args)
    {
        //创建组件
        WinForm winForm = new WinForm("Window窗口");
        Picture picture = new Picture("LOGO图片");
        Button loginBtn = new Button("登录");
        Button regBtn = new Button("注册");
        Frame frame = new Frame("FRAME1");
        Label lblUser = new Label("用户名");
        TextBox textBox = new TextBox("文本框");
        Label lblPassword = new Label("密码");
        PasswordTextBox passwordTextBox = new PasswordTextBox("密码框");
        CheckedBox checkedBox = new CheckedBox("复选框");
        //从示意图上看应该是Label，这里满足输出要求用TextBox
        TextBox txtRememberMe = new TextBox("记住用户名");
        LinkLabel linkLabel = new LinkLabel("忘记密码");
        //维护关系
        frame.addComponent(lblUser).addComponent(textBox).addComponent(lblPassword).addComponent(passwordTextBox).addComponent(checkedBox).addComponent(txtRememberMe).addComponent(linkLabel);
        winForm.addComponent(picture).addComponent(loginBtn).addComponent(regBtn).addComponent(frame);
        //打印组件
        winForm.print();
    }
}
