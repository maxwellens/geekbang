package org.geekbang.component;

import java.util.ArrayList;
import java.util.List;

/**
 * 组件组
 */
public abstract class ComponentGroup extends BaseComponent
{
    private List<Component> children = new ArrayList<>();

    public ComponentGroup(String name)
    {
        super(name);
    }

    /**
     * 添加子组件，返回组件组自己，支持链式调用
     *
     * @param component
     * @return
     */
    public ComponentGroup addComponent(Component component)
    {
        children.add(component);
        return this;
    }

    @Override
    public void print()
    {
        super.print();
        for (Component component : children)
        {
            component.print();
        }
    }
}
