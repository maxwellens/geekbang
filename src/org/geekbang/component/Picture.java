package org.geekbang.component;

/**
 * 图片
 */
public class Picture extends BaseComponent
{
    private String url;

    public Picture(String name)
    {
        super(name);
    }

    public void setUrl(String url)
    {
        this.url = url;
    }

}
