package org.geekbang.component;

/**
 * 按钮
 */
public class Button extends BaseComponent
{
    public Button(String name)
    {
        super(name);
    }

    public void press()
    {
        System.out.println(name + "点了一下");
    }

}
